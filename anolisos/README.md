# Anolis OS Base Container Images

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links
- [`8.6`, `latest`](https://gitee.com/anolis/docker-images/anolisos/Dockerfile)

# Supported architectures
- arm64, amd64

# Build reference

1. Download all images and update Dockerfile:

```shell
download.sh
```

2. Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/anolisos:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

#Anolis OS
Anolis OS is an open source Linux distribution issued by the OpenAnolis community. It is absolutely compatible with CentOS 8, supports multi-computing architecture, and provides a stable, high-performance, safe and reliable operating system.
