# MySQL

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`8.0.30-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/mysql/8.0.30/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/mysql:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `mysql` server instance
```shell
docker network create some-network
docker run --network some-network --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw --env MYSQL_USER=example-user --env MYSQL_PASSWORD=my_cool_secret -d openanolis/mysql
```
## Connect to MySQL from the MySQL command line client
```shell
docker run -it --network some-network --rm openanolis/mysql mysql -hsome-mysql -uexample-user -p
```

# MySQL
MySQL is the world's most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications, covering the entire range from personal projects and websites, via e-commerce and information services, all the way to high profile web properties including Facebook, Twitter, YouTube, Yahoo! and many more.
