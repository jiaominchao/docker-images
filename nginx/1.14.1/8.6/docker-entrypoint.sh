#!/bin/sh
set -e

# check if we have ipv6 available
if [ ! -f "/proc/net/if_inet6" ]; then \
    sed -i '/listen       \[::\]:80 default_server;/d' "/etc/nginx/nginx.conf"; \
fi

exec "$@"