# Nginx

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/nginx_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/nginx_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `nginx_keentune` server instance and set profile
```shell
docker run -ti -d --privileged="true"  --net=host openanolis/nginx_keentune /usr/sbin/init
docker exec -it $CONTAINER_ID /bin/bash nginx_keentune.sh
```

# Nginx
Nginx (pronounced "engine-x") is an open source reverse proxy server for HTTP, HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer, HTTP cache, and a web server (origin server).
