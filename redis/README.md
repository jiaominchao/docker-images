# Redis

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`5.0.3-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/redis/5.0.3/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/redis:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a redis instance

```shell
docker network create some-network
docker run --network some-network --name some-redis -d openanolis/redis
```

## connecting via `redis-cli`
```shell
docker run --network some-network -it --rm openanolis/redis redis-cli -h some-redis
```

# Redis
Redis is an open-source, networked, in-memory, key-value data store with optional durability.
