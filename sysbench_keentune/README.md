# Sysbench

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/sysbench_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/sysbench_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `sysbench_keentune` client instance and run stress test

```shell
docker run -it --net=host $IMAGE_ID /bin/bash mysql_read.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash mysql_write.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash mysql_rw.sh $SERVER_IP
docker run -it --net=host $IMAGE_ID /bin/bash mysql_noupdate.sh $SERVER_IP
```

# Sysbench
Sysbench is a Luajit-based, writable multithreaded benchmarking tool.  It is most commonly used for database benchmarking, but can also be used to create arbitrarily complex workloads that do not involve a database server 
