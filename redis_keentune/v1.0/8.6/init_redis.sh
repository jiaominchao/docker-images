redis_port=9400
instance_num=8
yum install numactl -y
systemctl stop redis
sed -i "s/^port .*/port $redis_port/" /etc/redis.conf;
sed -i "s/^bind.*/bind 0.0.0.0/" /etc/redis.conf; \
sed -i "s/^daemonize no/daemonize yes/" /etc/redis.conf; \
sed -i "s#^pidfile.*#pidfile /var/run/redis_$redis_port.pid#" /etc/redis.conf
numactl -C 0 /usr/bin/redis-server /etc/redis.conf

for ((i=1;i<$instance_num;i++))
do
        redis_port=$(($redis_port + 1))
        cp /etc/redis.conf /etc/redis.conf$i
        chown redis:root /etc/redis.conf$i
        mkdir -p /var/lib/redis$i

        sed -i "s/^port .*/port $redis_port/" /etc/redis.conf$i;
        sed -i "s#^pidfile.*#pidfile /var/run/redis_$redis_port.pid#" /etc/redis.conf$i;
        sed -i "s#^logfile.*#logfile /var/log/redis/redis$i.log#" /etc/redis.conf$i;
        sed -i "s#^dir.*#dir /var/lib/redis$i#" /etc/redis.conf$i

        numactl -C $i /usr/bin/redis-server /etc/redis.conf$i
done
