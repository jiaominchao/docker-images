# MariaDB

# Quick reference

- Maintained by: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)
- Where to get help: [OpenAnolis CloudNative SIG](https://openanolis.cn/sig/cloud-native)

# Supported tags and respective `Dockerfile` links

- [`10.3.35-8.6`, `latest`](https://gitee.com/anolis/docker-images/blob/master/mariadb/10.3.35/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

1. Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/mariadb:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage
```shell
docker run --detach --name some-mariadb --env MARIADB_USER=example-user --env MARIADB_PASSWORD=my_cool_secret --env MARIADB_ROOT_PASSWORD=my-secret-pw openanolis/mariadb
```

# MariaDB
MariaDB Server is one of the most popular database servers in the world. It’s made by the original developers of MySQL and guaranteed to stay open source. Notable users include Wikipedia, DBS Bank, and ServiceNow.

The intent is also to maintain high compatibility with MySQL, ensuring a library binary equivalency and exact matching with MySQL APIs and commands. MariaDB developers continue to develop new features and improve performance to better serve its users.
