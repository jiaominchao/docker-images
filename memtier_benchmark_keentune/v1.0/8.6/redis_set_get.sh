readonly REDIS_PORT=${REDIS_PORT:-9400}
readonly instance=8
cpu_cores=`cat /proc/cpuinfo | grep processor | wc -l`
process_cpu=$(($cpu_cores / $instance))
key_maximum=${key_maximum:-100000}
ratio="1:1"
server=$1
threads=${threads:-8}
test_time=${test_time:-60}
clients=${clients:-10}
pipeline=${pipeline:-1}
data_size=${data_size:-32}
key_maximum=${key_maximum:-100000}

client_pids=""
for i in $(seq 1 $instance); do
    cpu_start=$((($i - 1) * $process_cpu))
    cpu_end=$(($i * $process_cpu - 1))
    port=$(($REDIS_PORT + i - 1))
    client_cmd="numactl -C $cpu_start-$cpu_end memtier_benchmark  -s ${server} -p $port -t $threads --test-time=$test_time -c $clients --ratio=${ratio} --pipeline=$pipeline -d $data_size --key-maximum=$key_maximum"
    ${client_cmd} &
    client_pids="${client_pids} $!"
done

for pid in ${client_pids}; do wait ${pid} ; done
