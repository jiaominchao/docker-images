# MySQL

# Quick reference

- Maintained by: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)
- Where to get help: [OpenAnolis KeenTune SIG](https://openanolis.cn/sig/KeenTune)

# Supported tags and respective `Dockerfile` links

- [`v1.0-8.6`](https://gitee.com/anolis/docker-images/blob/master/mysql_keentune/v1.0/8.6/Dockerfile)

# Supported architectures

- arm64, x86-64

# Build reference

## Build multi-arch images and push to registry:
```shell
docker buildx build -t "openanolis/mysql_keentune:$VERSION" --platform linux/amd64,linux/arm64 . --push
```

# Usage

## Start a `mysql_keentune` server instance and set profile
```shell
docker run -ti -d --privileged="true"  --net=host openanolis/mysql_keentune /usr/sbin/init
docker exec -it $CONTAINER_ID /bin/bash mysql_keentune.sh
```

# MySQL
MySQL is the world's most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications, covering the entire range from personal projects and websites, via e-commerce and information services, all the way to high profile web properties including Facebook, Twitter, YouTube, Yahoo! and many more.
